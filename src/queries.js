module.exports = class Queries {
    constructor(client) {
        this.client = client;
    };

    async findFile(name) {
        const { rows: [ file ] } = await this.client.query('SELECT * FROM "file" WHERE "name" = $1',  [ name ]);

        return file;
    };
}
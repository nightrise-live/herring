if (process.env.NODE_ENV !== "production") require("dotenv").config();

const { Client } = require("pg");
const { createServer } = require("http");
const Queries = require("./queries");
const { readFile } = require("fs/promises");

const client = new Client({
    user: process.env.PG_USERNAME,
    password: process.env.PG_PASSWORD,
    host: process.env.PG_HOST,
    port: process.env.PG_PORT,
    database: process.env.PG_DB
});

const queries = new Queries(client);
const server = createServer();


server.on("request", async (req, res) => {
    const [ , ...paths ] = req.url.split("/");
    const file = paths[paths.length - 1];
    
    const fileInfo = await queries.findFile(file);
    if (!fileInfo || !fileInfo.accessible) {
        res.setHeader("Content-Type", "application/json")
        res.writeHead(404);
        res.end(JSON.stringify({
            success: false,
            error: "invaild file"
        }))
        return;
    };

    res.setHeader("Content-Type", fileInfo.ContentType);
    res.end(await readFile(fileInfo.path));
})

client.connect().then(() => {
    console.log("starting server");
    server.listen(process.env.PORT);
});